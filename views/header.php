<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
    <title>Framework MVC. Curso 2016/2017</title>
</head>
<body>
<div id="header">
<div id="title">Framework MVC. Curso 2016/2017</div>
<div>
    <a href="/index">Inicio</a>
    <a href="/articulo">Artículo</a>
    <a href="/user">Usuarios</a>
    <a href="/province">Provincias</a>
    <a href="/help">Ayuda</a>

    <?php echo $_SESSION['provincia'] ?>
</div>
</div>