<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Provincia localizada</h1>
    <ul>
    <?php foreach ($rows as $row): ?>
        <li><?php echo $row['id'] . ' - ' . $row['provincia'] ?></li>        
    <?php endforeach ?>
    </ul>

</div>

<?php
    require '../views/footer.php';
?>
