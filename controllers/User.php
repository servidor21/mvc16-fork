<?php
namespace Mvc\Controller;

require_once '../app/Controller.php';
require_once '../models/User.php';
/**
* Clase user para la gestión de usuarios
*/
class User extends \Mvc\App\Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //echo '<br>';
        //echo "En el controlador User";
        $this->_model = new \Mvc\Model\User;

    }

    //metodo con la lista de usarios
    public function index($page = 1)
    {
        $rows = $this->_model->get($page);

        require '../views/user/index.php';
    }

    public function new()
    {
        require '../views/user/new.php';
    }

    public function insert()
    {
        //validación ¿cliente o servidor?
        if ($_POST['password'] === $_POST['password2']) {
            $this->_model->insert();
            header('Location: /user');
        } else {
            header('Location: /user/new');
        }
    }
}
