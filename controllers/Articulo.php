<?php

/**
* Clase controladora de Artículo
*/

namespace Mvc\Controller;

class Articulo
{
    public $model;

    public function __construct()
    {
        require_once "../models/Articulo.php";
        $this->model = new \Mvc\Model\Articulo;
    }

    public function index()
    {
        $this->model->get();

        $rows = $this->model->getRows();
        include "../views/articulo/index.php";
    }
    public function delete($id)
    {
        //procedimiento de borrado
        $this->model->delete($id);
        //reenvio de url
        header('Location: /articulo/index');
//        echo "Borrar datos del registro actual ($id)";
    }
    public function new()
    {
        require '../views/articulo/new.php';
    }
    public function insert()
    {
//        echo "Insertar registro nuevo (en $_POST, envio desde new)";
        $this->model->new();
        header('Location: /articulo/index');
    }
    public function edit($id)
    {
        echo "Formulario de edición con datos antiguos";
        $row = $this->model->getById($id);
        include "../views/articulo/edit.php";
    }
    public function update()
    {
        $this->model->update();
        header('Location: /articulo/index');
    }
}
