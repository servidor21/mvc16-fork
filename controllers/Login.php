<?php
/**
* Controlador de login
*/
namespace Mvc\Controller;

require_once '../app/Controller.php';
require_once '../models/Login.php';

class Login extends \Mvc\App\Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->_model = new \Mvc\Model\Login;
    }

    public function index()
    {
        require "../views/login/index.php";
    }
}
