<?php

/**
* Modelo ActiveRecord User
*/
namespace Mvc\Model;

require_once '../app/Model.php';

class Province extends \Mvc\App\Model
{
    const PAGE_SIZE = 10;
    
    public function __construct()
    {
        parent::__construct();
        # code...
    }
    public function get($page)
    {
        $offset = ($page - 1) * $this::PAGE_SIZE;
        $size = $this::PAGE_SIZE;

        $sql = "select * from provincia limit $size offset $offset";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //var_dump($rows);
        return $rows;
    }

    public function getById($id)
    {
        $sql = "select * from provincia where id = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //var_dump($rows);
        return $rows;
    }

    public function delete($id)
    {
        $sql = "delete from provincia where id = :id";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->rowCount();
        return $result;
    }
}
