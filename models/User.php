<?php

/**
* Modelo ActiveRecord User
*/
namespace Mvc\Model;

require_once '../app/Model.php';

class User extends \Mvc\App\Model
{
    const PAGE_SIZE = 3;
    
    public function __construct()
    {
        parent::__construct();
        # code...
    }
    public function get($page)
    {
        $offset = ($page - 1) * $this::PAGE_SIZE;
        $size = $this::PAGE_SIZE;

        $sql = "select * from user limit $size offset $offset";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //var_dump($rows);
        return $rows;
    }

    public function insert()
    {
        $sql = "insert into user(name, surname, login, password) values(:name, :surname, :login, :password)";
        $stmt = $this->_pdo->prepare($sql);
        $stmt->bindValue(':name', $_POST['name']);
        $stmt->bindValue(':surname', $_POST['surname']);
        $stmt->bindValue(':login', $_POST['login']);
        $stmt->bindValue(':password', md5($_POST['password']));

        $stmt->execute();
    }

    public function pageCount()
    {
        //consultar registros
        //dividir por pageSize y redondear con ceil
        //return resultado
    }
}
